// Imports the Flutter Driver API.
import 'dart:io';
import 'dart:math';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:integration_test/integration_test.dart';
import 'package:test/test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  
  // Load environmental variables
  String imagePrefix =Platform.environment['imagePrefix'] ?? 'test_driver/screenshots/';
  late FlutterDriver driver;
  int random = Random.secure().nextInt(1024);
  int seq = 0;

  Future<void> takeScreenshot([String? name]) async {
    String fileName = name ?? '${random.toString()}-${(++seq).toString()}';
    fileName = '$imagePrefix$fileName.jpg';
    final List<int> pixels = await driver.screenshot();
    final File file = File(fileName);
    await file.writeAsBytes(pixels);
  }

  group('Counter App', () {
    
    // First, define the Finders and use them to locate widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys in step 1.
    final counterTextFinder = find.byValueKey('counter');
    final buttonFinder = find.byValueKey('increment');

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      driver.close();
    });

    test('starts at 0', () async {
      await takeScreenshot('starts_at_0');
      // Use the `driver.getText` method to verify the counter starts at 0.
      expect(await driver.getText(counterTextFinder), "0");
    });

    test('increments the counter', () async {
      // First, tap the button.
      await driver.tap(buttonFinder);

      await takeScreenshot('count_1');
      // Then, verify the counter text is incremented by 1.
      expect(await driver.getText(counterTextFinder), "1");
    });

    test('increments the counter during animation', () async {
      await driver.runUnsynchronized(() async {
        // First, tap the button.
        await driver.tap(buttonFinder);

        await takeScreenshot('count_2');
        // Then, verify the counter text is incremented by 1.
        expect(await driver.getText(counterTextFinder), "2");
      });
    });
  });
}