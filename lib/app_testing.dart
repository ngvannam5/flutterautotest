import 'package:autotest/models/favorites.dart';
import 'package:autotest/screens/favorites.dart';
import 'package:autotest/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppTesting extends StatelessWidget {
  const AppTesting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<Favorites>(
      create: (context) => Favorites(),
      child: MaterialApp(
        title: 'Testing Sample',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        routes: {
          HomePage.routeName: (context) => HomePage(),
          FavoritesPage.routeName: (context) => FavoritesPage(),
        },
        initialRoute: HomePage.routeName,
      ),
    );
  }
}
