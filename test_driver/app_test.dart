// Imports the Flutter Driver API.
import 'dart:io';
import 'dart:math';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  // Load environmental variables
  String imagePrefix = Platform.environment['imagePrefix'] ?? 'test_driver/screenshots/';
  late FlutterDriver driver;
  int random = Random.secure().nextInt(1024);
  int seq = 0;

  Future<void> takeScreenshot([String? name]) async {
    String fileName = name ?? '${random.toString()}-${(++seq).toString()}';
    fileName = '$imagePrefix$fileName.jpg';
    final List<int> pixels = await driver.screenshot();
    final File file = File(fileName);
    await file.writeAsBytes(pixels);
  }

  group('App Testing', () {
    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      driver.close();
    });

    test('Favorites operations test', () async {
      final iconKeys = [
        'icon_0',
        'icon_1',
        'icon_2',
      ];
      for (var icon in iconKeys) {
        await driver.tap(find.byValueKey('$icon'));
        await driver.waitFor(find.byValueKey('$icon'), timeout: Duration(seconds: 1));
        expect(await driver.getText(find.byValueKey('home_page_snackbar')), 'Added to favorites.');
        await takeScreenshot('added_$icon');
      }

      await driver.tap(find.byValueKey('button.favorites'));
      await driver.waitFor(find.byValueKey('favorites_page'), timeout: Duration(seconds: 1));

      final removeIconKeys = [
        'remove_icon_0',
        'remove_icon_1',
        'remove_icon_2',
      ];

      for (final iconKey in removeIconKeys) {
        await driver.tap(find.byValueKey('$iconKey'));
        expect(await driver.getText(find.byValueKey('favorites_page_snackbar')), 'Removed from favorites.');
        await takeScreenshot('removed_$iconKey');
      }
    });
  });

  // group('Counter App', () {
  //   // First, define the Finders and use them to locate widgets from the
  //   // test suite. Note: the Strings provided to the `byValueKey` method must
  //   // be the same as the Strings we used for the Keys in step 1.
  //   final counterTextFinder = find.byValueKey('counter');
  //   final buttonFinder = find.byValueKey('increment');

  //   // Connect to the Flutter driver before running any tests.
  //   setUpAll(() async {
  //     driver = await FlutterDriver.connect();
  //   });

  //   // Close the connection to the driver after the tests have completed.
  //   tearDownAll(() async {
  //     driver.close();
  //   });

  //   // test('starts at 0', () async {
  //   //   await takeScreenshot('starts_at_0');
  //   //   // Use the `driver.getText` method to verify the counter starts at 0.
  //   //   expect(await driver.getText(counterTextFinder), "0");
  //   // });

  //   test('increments the counter', () async {
  //     // First, tap the button.
  //     await driver.tap(buttonFinder);

  //     await takeScreenshot('count_1');
  //     // Then, verify the counter text is incremented by 1.
  //     expect(await driver.getText(counterTextFinder), "1");
  //   });

  //   test('increments the counter during animation', () async {
  //     await driver.runUnsynchronized(() async {
  //       // First, tap the button.
  //       await driver.tap(buttonFinder);

  //       await takeScreenshot('count_2');
  //       // Then, verify the counter text is incremented by 1.
  //       expect(await driver.getText(counterTextFinder), "2");
  //     });
  //   });
  // });
}
